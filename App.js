import 'react-native-gesture-handler'
import React, { useState } from 'react'
import { useFonts } from '@use-expo/font'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import globals from 'globals/styles'

import Loading from './components/Loading'
import Main from './components/Main'
import Channel from './components/Main/Channel'
import Item from './components/Main/Channel/Item'
import Logobar from './components/Main/Logobar'

export default function App () {
  const [isLoading, setIsLoading] = useState(false)

  const Stack = createStackNavigator()

  const [fontsLoaded] = useFonts({
    'HELVETICA-NEUE-REGULAR': require('assets/fonts/HelveticaNeueMedium.ttf'),
    'HELVETICA-NEUE-BOLD': require('assets/fonts/HelveticaNeueBold.ttf')
  })

  const headerStyle = {
    backgroundColor: globals.colors.purple,
    shadowColor: 'transparent'
  }

  if (fontsLoaded) { // Simulates loading other possible resources, latency, etc.
    setTimeout(() => {
      setIsLoading(true)
    }, 2000)
  }

  if (!isLoading) {
    return <Loading />
  } else {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Home'>
          <Stack.Screen
            name='Main'
            component={Main}
            options={{
              headerTitleAlign: 'center',
              headerTitle: () => <Logobar />,
              headerStyle: {
                ...headerStyle
              }
            }}
          />
          <Stack.Screen
            name='Channel'
            component={Channel}
            options={{
              headerTitleAlign: 'center',
              headerTitle: () => <Logobar />,
              headerStyle: {
                ...headerStyle
              },
              headerBackTitle: ' ',
              headerTintColor: globals.colors.white

            }}
          />
          <Stack.Screen
            name='Item'
            component={Item}
            options={
              ({ route }) => ({
                headerTitleAlign: 'center',
                headerTitle: props => <Logobar channel={route.params.channel} />,
                headerStyle: {
                  ...headerStyle
                },
                headerBackTitle: ' ',
                headerTitleAllowFontScaling: true,
                headerTintColor: globals.colors.white
              })

            }
          />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}
