# README #

React Native Demo. Includes listing, navigation, buttons, state, etc. Uses the Expo platform

### How do I get set up? ###

1. Clone
2. `yarn` to set up dependencies
2. `yarn start` to run dev environment
3. Download the Expo app from Apple store or Google Play
4. For iOS scan the QR code with the Camera app. For Android use the Expo app directly

### Who do I talk to? ###

* For contacts please send email to nanttis@outlook.com