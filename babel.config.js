module.exports = function (api) {
  api.cache(true)
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      ['module-resolver', { // Aliasing for easier path management
        root: ['.'],
        alias: {
          assets: './assets',
          globals: './components/globals'
        }
      }
      ]
    ]
  }
}
