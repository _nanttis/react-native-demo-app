import React from 'react'
import { ActivityIndicator, ImageBackground, StyleSheet, View, SafeAreaView, Image } from 'react-native'

export default function Loading () {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={images.bg} style={styles.bg}>
        <View style={styles.upperBlock}>
          <Image source={images.splash} style={styles.splash} />
        </View>
        <View style={styles.lowerBlock}>
          <ActivityIndicator size='large' color='#ccc' />
        </View>
      </ImageBackground>
    </SafeAreaView>
  )
}

const images = {
  bg: require('assets/bg.png'),
  splash: require('assets/splash_logo.png')
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  upperBlock: {
    flex: 3
  },
  lowerBlock: {
    flex: 1
  },
  bg: {
    flex: 1,
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  splash: {
    flex: 1,
    height: undefined,
    width: 160,
    resizeMode: 'contain'
  }
})
