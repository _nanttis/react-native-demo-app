import React from 'react'
import { View, StyleSheet, Text, ImageBackground, Image, ScrollView } from 'react-native'
import globals from 'globals/styles'
import normalize from '../../../globals/normalize'

export default function Item (props) {
  const { params } = props.route

  return (
    <View style={styles.container}>
      <ImageBackground style={styles.header} source={require('assets/background_article_header.png')}>
        <Text style={styles.channel}>{params.title}</Text>
      </ImageBackground>
      <View style={styles.article}>
        <View style={styles.details}>
          <View style={styles.detailsLeft}>
            <Image style={styles.imageSource} source={require('assets/icon_list_source.png')} />
            <Text style={styles.source}>{params.source}</Text>
            <Image style={styles.imageClock} source={require('assets/icon_list_time.png')} />
            <Text style={styles.date}>{params.date}</Text>
          </View>
          <View style={styles.detailsRight}>
            <View style={styles.channelTitleView}>
              <Text style={styles.channelTitle}>{params.channel}</Text>
            </View>
          </View>
        </View>
        <ScrollView>
          <Text style={styles.articleText}>
            {params.article}
          </Text>
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start'
  },
  header: {
    flex: 6,
    justifyContent: 'flex-end',
    alignItems: 'flex-start'
  },
  article: {
    flex: 8,
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
  channel: {
    ...globals.styles.TitleCategory,
    fontSize: normalize(16),
    padding: 20,
    textShadowColor: '#000',
    textShadowRadius: 3
  },
  details: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20
  },
  source: {
    ...globals.styles.Description
  },
  date: {
    ...globals.styles.Description
  },
  imageSource: {
    marginEnd: 10
  },
  imageClock: {
    marginStart: 30,
    marginEnd: 10
  },
  channelTitle: {
    ...globals.styles.Description,
    fontSize: normalize(8),
    textTransform: 'uppercase'
  },
  channelTitleView: {
    paddingBottom: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#ddd'
  },
  detailsLeft: {
    flex: 2,
    alignItems: 'center',
    flexDirection: 'row'
  },
  detailsRight: {
    flex: 1,
    alignItems: 'flex-end'
  },
  articleText: {
    ...globals.styles.Body,
    paddingHorizontal: 20
  }
})
