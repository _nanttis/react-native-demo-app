import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Dimensions, ImageBackground, Image, FlatList, ActivityIndicator } from 'react-native'
import globals from 'globals/styles'

const w = Dimensions.get('window')

export default function Channel (props) {
  const channel = props.route.params.channel

  return (
    <View style={styles.container}>
      <ImageBackground source={require('assets/Background_channelheader_Science.png')} style={styles.header}>
        <Text style={styles.channel}>{channel} CHANNEL</Text>
        <TouchableOpacity style={styles.button}><Text style={styles.followers}>Following</Text></TouchableOpacity>
        <Text style={styles.followers}>234K Followers</Text>
      </ImageBackground>
      <View style={styles.list}>
        <List {...props} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#eee'
  },
  header: {
    flex: 4,
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  list: {
    flex: 10,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  channel: {
    ...globals.styles.TitleChannel
  },
  button: {
    backgroundColor: globals.colors.pink,
    borderRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  followers: {
    ...globals.styles.TitleCategory
  }
})

const List = (props) => {
  const [items, setItems] = useState(null)

  useEffect(() => {
    // Usually Axios or fetch inside a useEffect or lifecycle in class component
    setTimeout(() => { // Simulating some latency
      const data = require('./data.json')
      setItems(data)
    }, 2000)
  })

  const styles = StyleSheet.create({
    item: {
      marginBottom: 2,
      padding: 20,
      width: w.width,
      backgroundColor: globals.colors.white
    },
    title: {
      ...globals.styles.BodyBold
    },
    details: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 10
    },
    source: {
      ...globals.styles.Description
    },
    date: {
      ...globals.styles.Description

    },
    imageSource: {
      marginEnd: 10
    },
    imageClock: {
      marginStart: 30,
      marginEnd: 10
    }
  })

  if (items) {
    return (
      <FlatList
        data={items}
        keyExtractor={item => item.title}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            style={styles.item}
            key={index}
            onPress={() => props.navigation.navigate('Item', { // Also would include picture and other props
              title: item.title,
              source: item.source,
              date: item.date,
              article: item.article,
              channel: props.route.params.channel
            })}
          >
            <View>
              <Text style={styles.title}>{item.title}</Text>
            </View>
            <View style={styles.details}>
              <Image style={styles.imageSource} source={require('assets/icon_list_source.png')} />
              <Text style={styles.source}>{item.source}</Text>

              <Image style={styles.imageClock} source={require('assets/icon_list_time.png')} />
              <Text style={styles.date}>{item.date}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    )
  } else {
    return (
      <ActivityIndicator style={{ flex: 1 }} />
    )
  }
}
