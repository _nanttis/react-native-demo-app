import React from 'react'
import { TouchableHighlight, StyleSheet, Dimensions, Text, Image, ImageBackground } from 'react-native'
import { FlatGrid } from 'react-native-super-grid'
import globals from 'globals/styles'

const w = Dimensions.get('window')

export default function List ({ navigation }) {
  return (
    <FlatGrid
      itemDimension={w.width / 2 - 1} // Must be slightly less than half
      items={list}
      style={styles.gridView}
      spacing={0}
      renderItem={({ item, index }) => (
        <TouchableHighlight
          style={styles.item}
          key={index}
          onPress={() => navigation.navigate('Channel', { channel: item.channel })}
        >
          <ImageBackground style={styles.image} source={item.source}>
            <Text style={styles.channel}>{item.channel}</Text>
          </ImageBackground>
        </TouchableHighlight>
      )}
    />
  )
}

const list = [
  { channel: 'Fashion', source: require('assets/Thumbnail_channel_Fashion.png') },
  { channel: 'Science', source: require('assets/Thumbnail_channel_Science.png') },
  { channel: 'Auto', source: require('assets/Thumbnail_channel_Auto.png') },
  { channel: 'Technology', source: require('assets/Thumbnail_channel_Technology.png') },
  { channel: 'Entertainment', source: require('assets/Thumbnail_channel_Entertainment.png') },
  { channel: 'Environment', source: require('assets/Thumbnail_channel_Environment.png') },
  { channel: 'Finance', source: require('assets/Thumbnail_channel_Finance.png') },
  { channel: 'Travel', source: require('assets/Thumbnail_channel_Travel.png') }
]

const styles = StyleSheet.create({
  gridView: {
    flex: 1
  },
  image: {
    flex: 1,
    width: w.width / 2,
    height: 173 / 188 * w.width / 2, // Scale image to fit
    resizeMode: 'cover'
  },
  item: {
    justifyContent: 'flex-end'
  },
  channel: {
    position: 'absolute',
    bottom: 0,
    padding: 20,
    textShadowRadius: 5,
    textShadowColor: '#000',
    ...globals.styles.TitleThumbnail
  }
})
