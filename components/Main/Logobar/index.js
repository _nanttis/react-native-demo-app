import React from 'react'
import { Image, StyleSheet, Text } from 'react-native'
import globals from 'globals/styles'
import normalize from 'globals/normalize'

export default function Logobar (props) {
  const content = props.channel
    ? <Text style={styles.text}>{props.channel} CHANNEL</Text>
    : <Image source={images.logo} style={styles.logo} />

  return content
}

const images = {
  logo: require('assets/Icon_navbar_Logo.png')
}

const styles = StyleSheet.create({
  logo: {
    resizeMode: 'contain',
    flex: 1,
    margin: 10
  },
  text: {
    ...globals.styles.TitleChannel,
    fontSize: normalize(14) // Font resizing here. Not in specs but overflows otherwise badly.
  }
})
