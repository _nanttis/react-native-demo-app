import React, { useState } from 'react'
import { TouchableOpacity, StyleSheet, View, Text, SafeAreaView } from 'react-native'
import List from './List'
import globals from 'globals/styles'

export default function Main ({ navigation }) {
  const buttons = [
    'Following',
    'Popular',
    'Explore'
  ]

  const [buttonActive, setButtonActive] = useState(0)

  const _handlePress = (props) => {
    setButtonActive(props.id)
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        {buttons.map((item, index) => {
          return (
            <NavButton
              title={item}
              key={index}
              id={index}
              active={() => buttonActive === index}
              onPress={_handlePress}
            />
          )
        })}

      </View>
      <View style={styles.list}>
        <List navigation={navigation} />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  navbar: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#162D4E'
  },
  list: {
    flex: 8
  }
})

const NavButton = (props) => {
  const styles = StyleSheet.create({
    button: {
      borderWidth: 1,
      borderColor: 'transparent',
      borderRadius: 30
    },
    activeButton: {
      borderWidth: 1,
      borderColor: globals.colors.pink,
      borderRadius: 30
    },
    text: {
      paddingHorizontal: 20,
      paddingVertical: 10,
      ...globals.styles.TitleCategory
    }
  })

  const buttonStyle = props.active() ? styles.activeButton : styles.button

  return (
    <TouchableOpacity
      style={buttonStyle} onPress={() => {
        props.onPress(props)
      }}
    ><Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  )
}
