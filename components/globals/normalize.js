import { PixelRatio, Dimensions } from 'react-native'

const { width: SCREEN_WIDTH } = Dimensions.get('window')

// Smallest iPhone width (SE)
const scale = SCREEN_WIDTH / 320

export default function normalize (size) {
  const newSize = size * scale
  return Math.round(PixelRatio.roundToNearestPixel(newSize))
}
