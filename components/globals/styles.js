import { StyleSheet } from 'react-native'
import normalize from 'globals/normalize' // Here we could use "react-native-responsive-fontsize"

const colors = {
  purple: 'rgb(161,117,211)',
  pink: 'rgb(230,82,138)',
  black: 'rgb(29,29,38)',
  white: 'rgb(255,255,255)'
}

export default {
  colors: {
    ...colors
  },
  styles: StyleSheet.create({
    TitleThumbnail: {
      fontFamily: 'HELVETICA-NEUE-REGULAR',
      fontSize: normalize(10),
      textTransform: 'uppercase',
      color: colors.white
    },
    TitleChannel: {
      fontFamily: 'HELVETICA-NEUE-BOLD',
      fontSize: normalize(20),
      textTransform: 'uppercase',
      color: colors.white
    },
    TitleCategory: {
      fontFamily: 'HELVETICA-NEUE-BOLD',
      fontSize: normalize(12),
      color: colors.white
    },
    Body: {
      fontFamily: 'HELVETICA-NEUE-REGULAR',
      fontSize: normalize(14),
      color: colors.black
    },
    BodyBold: {
      fontFamily: 'HELVETICA-NEUE-BOLD',
      fontSize: normalize(14),
      color: colors.black
    },
    Description: {
      fontFamily: 'HELVETICA-NEUE-REGULAR',
      fontSize: normalize(12),
      color: colors.black
    }
  })
}
